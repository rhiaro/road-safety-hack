(function(){

  // Check the rest of the page has loaded before starting everything
  document.addEventListener('DOMContentLoaded', init);

  // Trigger everything
  function init(){
    get_location();
  }

  // Location fail: Handle error
  function handle_error(err) {
    if (err.code == 1) {
      alert('You said no');
    }else if(err.code == 2){
      alert('Could not get loaction');
    }else if(err.code == 3){
      alert('Time out. Try again.');
    }else{
      alert('Something unknown went wrong');
    }
  }

  // Get location of user
  // geolocation.getCurrentPosition(do_stuff) passes the position found to the function do_stuff
  function get_location() {
    navigator.geolocation.watchPosition(do_stuff, handle_error,{enableHighAccuracy:true});
  }

  // Do interesting things
  function do_stuff(position) {
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;

    console.log("Accuracy:" + position.coords.accuracy);
    document.getElementById("accuracy").innerHTML = position.coords.accuracy; 

    // Get display size
    var h = window.innerHeight;

    // See which accidents happened within a certain distance of user
    var radius = 0.1; // Km
    var danger_zones = 0;
    var slight = 0;
    var serious = 0;
    var fatal = 0;

    // Check each data point
    for(var i=0; i < data.features.length; i++){
      var check_lat = data.features[i].properties.latitude;
      var check_lon = data.features[i].properties.longitude;

      // See if accident happened within certain distance of user
      if(point_in_range(latitude, longitude, check_lat, check_lon, radius)){
        // Count accidents
        danger_zones++;
        // Get severity
        var severity = data.features[i].properties.severity;
        // Count slight accidents
        if(severity == "Slight"){
          slight++;
        }
        // Count serious accidents
        if(severity == "Serious"){
          serious++;
        }
        // Count fatal accidents
        if(severity == "Fatal"){
          fatal++;
        }
      }
    }
    // Change colour size according to accident severity
    var sev = "should be safe!";
    if(slight > 0){
      sev = "mostly slight";
    }
    if(serious > 0 && serious >= slight){
      sev = "mostly serious";
    }
    if(fatal >= serious && fatal >= slight && fatal > 0){ // red
      sev = "mostly fatal";
    }

    // Loading...
    var load = document.getElementById("load");
    if(load) { load.parentNode.removeChild(load); }

    // Resize colour blocks
    var slight_h = 0;
    var serious_h = 0;
    var fatal_h = 0;
    if(slight > 0) {slight_h = slight / danger_zones * 100;}
    if(serious > 0) {serious_h = serious / danger_zones * 100;}
    if(fatal > 0) {fatal_h = fatal / danger_zones * 100;}
    document.getElementById("slight").style.height = slight_h + "%";
    document.getElementById("serious").style.height = serious_h + "%";
    document.getElementById("fatal").style.height = fatal_h + "%";

    console.log(slight_h);
    console.log(serious_h);
    console.log(fatal_h);

    // Print infos
    var radius_m = radius * 100;
    document.getElementById("radius").innerHTML = radius_m + "m"; 
    document.getElementById("acc").innerHTML = danger_zones; 
    document.getElementById("sev").innerHTML = sev; 
  }


  // Check distance between 2 points
  function distance(lat1, lon1, lat2, lon2, unit) {

    //Units: 'M' is statute miles, 'K' is kilometers (default), 'N' is nautical miles  

    var radlat1 = Math.PI * lat1/180
    var radlat2 = Math.PI * lat2/180
    var radlon1 = Math.PI * lon1/180
    var radlon2 = Math.PI * lon2/180
    var theta = lon1-lon2
    var radtheta = Math.PI * theta/180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist)
    dist = dist * 180/Math.PI
    dist = dist * 60 * 1.1515
    if (unit=="K") { dist = dist * 1.609344 }
    if (unit=="N") { dist = dist * 0.8684 }
    return dist
  }

  // Check if two points are within a certain distance of each other
  function point_in_range(lat1, lon1, lat2, lon2, radius){
    var dist = distance(lat1, lon1, lat2, lon2, "K");
    if(dist <= radius){
      return true;
    }else{
      return false;
    }
  }


})();